using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Player : MonoBehaviour
{
    public static Player player; //Static members shouldn't be used like this but this is a quick prototype.

    public Hitbox hitbox;
    public ParticleSystem damageEffect;
    public PlayerCamera cameraTransform;

    public float movementSpeed = 5f;
    public float focusMovementSpeed = 5f;
    public float jumpForce = 5f;

    public float focusAlpha = .25f;
    private float currentAlpha = 1;
    private float targetAlpha = 1;
    public Color hurtColor = Color.red;
    public float hurtLerpWeight = 10f;

    private float yVelocity = 0;
    private bool _onGround = false;
    public bool onGround
    {
        get => _onGround;
        set
        {
            if (_onGround == value) return;
            if (_onGround) EnteringGround?.Invoke(this, EventArgs.Empty);
            else LeavingGround?.Invoke(this, EventArgs.Empty);
            _onGround = value;
        }
    }

    private CharacterController cc;
    private Vector3 movementInput = Vector3.zero;
    private float jumpRequested = 0;

    public enum PlayerState
    {
        Normal,
        Focus
    }
    private PlayerState _currentPlayerState = PlayerState.Normal;
    private PlayerState currentPlayerState
    {
        set
        {
            if (value == _currentPlayerState)
            {
                return;
            }

            if (value == PlayerState.Normal) //When transitioning to normal
            {
                targetAlpha = 1;
                EnteringNormal?.Invoke(this, EventArgs.Empty);
            }
            else
            {
                targetAlpha = focusAlpha; //Else
                EnteringFocus?.Invoke(this, EventArgs.Empty);
            }
            _currentPlayerState = value;
        }
        get
        {
            return _currentPlayerState;
        }
    }

    public PlayerState GetPlayerState()
    {
        return _currentPlayerState;
    }

    private Vector3 center;
    private float radius = 0.5f;
    private float height = 2;


    public event EventHandler LeavingGround;
    public event EventHandler EnteringGround;
    public event EventHandler EnteringNormal;
    public event EventHandler EnteringFocus;

    private void Awake()
    {
        player = this;

        hitbox.OnHit += OnHit;
        cc = GetComponent<CharacterController>();
        center = cc.center;
        radius = cc.radius;
        height = cc.height;
    }

    void OnHit(object other, EventArgs args)
    {
        damageEffect.Play();

        Color materialColor = GetComponent<MeshRenderer>().material.color;
        materialColor = new Color(hurtColor.r, hurtColor.g, hurtColor.b, materialColor.a);
        GetComponent<MeshRenderer>().material.color = materialColor;
    }

    void Update()
    {
        movementInput = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Transversal"), Input.GetAxis("Vertical"));
        if (jumpRequested > 0)
        {
            jumpRequested -= Time.deltaTime;
        }
        //if (onGround && Input.GetButtonDown("Jump")){
        if (Input.GetButtonDown("Jump"))
        {
                jumpRequested = .2f;
        }
        if (!onGround)
        {
            if (Input.GetButtonDown("Focus")){
                currentPlayerState = PlayerState.Focus;
            }
            else if (!Input.GetButton("Focus") && currentPlayerState == PlayerState.Focus){
                currentPlayerState = PlayerState.Normal;
                yVelocity = 0;
            }
        }

        SetAlpha();
        SetColor();
        SetRotation();
    }

    void SetAlpha()
    {
        currentAlpha = Mathf.MoveTowards(currentAlpha, targetAlpha, Time.deltaTime);
        Color materialColor = GetComponent<MeshRenderer>().material.color;
        materialColor.a = currentAlpha;
        GetComponent<MeshRenderer>().material.color = materialColor;
    }

    void SetColor()
    {
        Color materialColor = GetComponent<MeshRenderer>().material.color;
        Color targetColor = new Color(1, 1, 1, materialColor.a);
        materialColor = Color.Lerp(materialColor, targetColor, Time.deltaTime * hurtLerpWeight);
        GetComponent<MeshRenderer>().material.color = materialColor;
    }

    void SetRotation()
    {
        if (currentPlayerState == PlayerState.Normal)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        else
        {
            transform.LookAt(transform.position + cameraTransform.transform.forward);
        }
    }

    void FixedUpdate()
    {
        Vector3 movement = Vector3.zero;
        switch (currentPlayerState)
        {
            case PlayerState.Normal:
                movement = NormalMove();
                break;
            case PlayerState.Focus:
                movement = FocusMove();
                break;
        }

        CollisionFlags collision = cc.Move(movement);
        if ((collision & CollisionFlags.Below) != 0)
        {
            onGround = true;
            yVelocity = 0;
            currentPlayerState = PlayerState.Normal;
        }
        else
        {
            onGround = false;
        }
        yVelocity += Physics.gravity.y * Time.fixedDeltaTime;

        if (jumpRequested > 0)
        {
            //cc.SimpleMove(Vector3.zero);
            currentPlayerState = PlayerState.Normal;
            onGround = false;
            yVelocity = jumpForce;
            jumpRequested = 0;
        }
        
    }

    private Vector3 NormalMove()
    {
        Vector3 cameraForward = Vector3.Scale(cameraTransform.transform.forward, new Vector3(1, 0, 1)).normalized;
        Vector3 movementDirection = cameraForward * movementInput.z + cameraTransform.transform.right * movementInput.x;
        return (movementDirection * movementSpeed + Vector3.up * yVelocity) * Time.fixedDeltaTime;

    }
    private Vector3 FocusMove()
    {
        Vector3 movementDirection = cameraTransform.transform.up * movementInput.y + cameraTransform.transform.right * movementInput.x + cameraTransform.transform.forward * movementInput.z;
        return (movementDirection * focusMovementSpeed) * Time.fixedDeltaTime;
    }

    void Start()
    {

    }

    //private Vector3 NormalMove()
    //{
    //    Vector3 cameraForward = -Vector3.Scale(cameraTransform.currentDirection, new Vector3(1, 0, 1)).normalized;
    //    Vector3 cameraRight = Quaternion.Euler(0, 90, 0) * cameraForward;
    //    Vector3 movementDirection = cameraForward * movementInput.z + cameraRight * movementInput.x;
    //    return (movementDirection * movementSpeed + Vector3.up * yVelocity) * Time.fixedDeltaTime;

    //}
    //private Vector3 FocusMove()
    //{
    //    Vector3 cameraForward = -Vector3.Scale(cameraTransform.currentDirection, new Vector3(1, 0, 1)).normalized;
    //    Vector3 cameraRight = Quaternion.Euler(0, 90, 0) * cameraForward;
    //    Vector3 cameraUp = Quaternion.Euler(cameraRight.x * 90, cameraRight.y * 90, cameraRight.z * 90) * cameraForward;

    //    Vector3 movementDirection = cameraUp * movementInput.z + cameraRight * movementInput.x;
    //    return (movementDirection * focusMovementSpeed) * Time.fixedDeltaTime;
    //}
}
