using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FocusActivator : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Player.player.LeavingGround += Deactivate;
        Player.player.EnteringGround += Activate;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Activate(object emmiter, EventArgs args)
    {
        GetComponent<BulletSpawner>().enabled = true;
    }
    void Deactivate(object emmiter, EventArgs args)
    {
        GetComponent<BulletSpawner>().enabled = false;
    }
}
