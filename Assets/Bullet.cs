using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 3f;
    public float lifeTime = 40f;

    public GameObject prefabSpark;

    void FixedUpdate()
    {
        GetComponent<Rigidbody>().MovePosition(transform.position + transform.forward * speed * Time.fixedDeltaTime);

        if (lifeTime <= 0)
        {
            Die();
        }
        lifeTime -= Time.fixedDeltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        Die();
    }

    void Die()
    {
        Destroy(gameObject);
        Instantiate(prefabSpark, transform.position, Quaternion.identity);
    }
}
