using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blaster : BulletSpawner
{
    public int maxSteps = 5;
    public int stepMultiplier = 4;

    public override void SpawnBullet()
    {
        transform.Rotate(transform.up, Random.value);
        Vector3 currentDirection = Vector3.up;
        for(int step = 0; step < maxSteps; step++)
        {
            currentDirection = Vector3.RotateTowards(currentDirection, transform.forward, (Mathf.PI/2)/maxSteps, 0);
            int h_steps = stepMultiplier * (step + 1);
            for (int i_step = 0; i_step < h_steps; i_step++)
            {
                Vector3 horizontalDirection = Quaternion.Euler(0, 360 / h_steps * i_step, 0) * currentDirection;
                GameObject newBullet = Instantiate(bulletPrefab, transform.position, transform.rotation);
                newBullet.transform.LookAt(newBullet.transform.position + horizontalDirection);
            }
        }
    }
}
