using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    public LayerMask groundLayer;
    public Transform playerTransform;
    public Vector2 playerDistance;
    public Vector2 playerFocusDistance;

    public float smooth = 2f;

    Vector3 normalDirection = -Vector3.forward;
    Vector3 focusDirection = -Vector3.forward;
    Player.PlayerState currentPlayerState = Player.PlayerState.Normal;
    float smoothState = 0; //En 0, est� en Normal Mode. En 1, est� en Focus Mode.

    void Start()
    {
        
    }

    void Update()
    {
        if (currentPlayerState != playerTransform.GetComponent<Player>().GetPlayerState())
        {
            currentPlayerState = playerTransform.GetComponent<Player>().GetPlayerState();

            if (currentPlayerState == Player.PlayerState.Focus)
            {
                focusDirection = -transform.forward;
            }
        }

        if (currentPlayerState == Player.PlayerState.Focus)
        {
            smoothState = Mathf.MoveTowards(smoothState, 1, Time.deltaTime * smooth);
        }
        else
        {
            smoothState = Mathf.MoveTowards(smoothState, 0, Time.deltaTime * smooth);
        }
        
        Vector3 normalTargetPos = GetCameraNormalModeTargetPos();
        Vector3 focusTargetPos = GetCameraFocusModeTargetPos();
        Vector3 targetPos = Vector3.zero;

        Vector3 offset = playerTransform.up * playerFocusDistance.y * smoothState;

        transform.position = Vector3.Lerp(normalTargetPos, focusTargetPos, smoothState);
        transform.LookAt(playerTransform.position + offset);
    }

    Vector3 GetCameraNormalModeTargetPos()
    {
        normalDirection = Quaternion.Euler(0, Input.GetAxis("CameraHorizontal"), 0) * normalDirection;
        Vector3 targetPosition = GetPlayersFloor() + (Vector3.Scale(normalDirection, new Vector3(1, 0, 1))).normalized * playerDistance.x + Vector3.up * playerDistance.y;
        //transform.position = Vector3.Lerp(transform.position, targetPosition, smooth * Time.deltaTime);
        return targetPosition;
        
    }

    Vector3 GetCameraFocusModeTargetPos()
    {
        focusDirection = Quaternion.Euler(Vector3.up * Input.GetAxis("CameraHorizontal") -transform.right * Input.GetAxis("CameraVertical")) * focusDirection;
        Vector3 targetPosition = playerTransform.position + focusDirection.normalized * playerFocusDistance.x;

        //transform.position = Vector3.Lerp(transform.position, targetPosition, smooth * Time.deltaTime);
        return targetPosition;
    }

    Vector3 GetPlayersFloor()
    {
        RaycastHit hit;
        if (Physics.Raycast(playerTransform.position, Vector3.down, out hit, 10000, groundLayer))
        {
            return hit.point;
        }
        return playerTransform.position;
    }
}