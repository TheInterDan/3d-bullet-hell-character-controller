using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Hitbox : MonoBehaviour
{
    public event EventHandler OnHit;

    private void OnTriggerEnter(Collider other)
    {
        OnHit(this, EventArgs.Empty);
    }
}
