using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpawner : MonoBehaviour
{
    public GameObject bulletPrefab;
    public float cooldown = 1f;

    private float cooldownTimer;

    void Start()
    {
        cooldownTimer = cooldown;
    }

    void Update()
    {
        if (cooldownTimer <= 0)
        {
            SpawnBullet();
            cooldownTimer = cooldown;
        }

        cooldownTimer -= Time.deltaTime;
    }

    public virtual void SpawnBullet()
    {
        Instantiate(bulletPrefab, transform.position, transform.rotation);
    }
}
