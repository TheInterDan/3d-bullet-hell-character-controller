# 3D Bullet Hell Controller

## About

This is a short demo with the purpose of showcasing how a 3rd Person Character Controller could work on a 3D Bullet Hell type of game. 

## Controls

(The game works both on keyboard and on XBox Gamepad)
Mouse / Right Stick - Camera movement (normally only moves horizontally, on Focus Mode it moves also vertically)
- **W A S D / Left Stick** - Movement
- **Space / A** - Jump (can jump mid-air indefinitely)
- **Shift / Any Bumper** (Hold during Mid-air) - Focus mode (user can fly at a slow pace for precise maneuvering)
- **Q / Left Trigger** - Move Down relative to the camera during Focus Mode
- **E / Right Trigger** - Move Up relative to the camera during Focus Mode

## Timelapse

A timelapse for the project can be found [here](https://drive.google.com/file/d/1_UvE0lXYG3G6O956TZItrcQVCsysGt1I/view?usp=drive_link), from conceptualizaion to a minimal viable sample of the mechanic.

## Comments

- In retrospect, I think that using the Unity Event system instead of the C# one would have been a better option, specially this being just a quick prototype.
